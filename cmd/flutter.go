package cmd

import (
	"os"
	"os/exec"
	"os/user"

	"github.com/fatih/color"
	"github.com/mcuadros/go-version"
)

type FlutterSdk struct {
	version	string
}

const defaultChannel = "stable"

func (flutterSdk *FlutterSdk) versionDefined() bool {
	return flutterSdk.version != ""
}

func (flutterSdk *FlutterSdk) home() string {
	fwrapHome := os.Getenv("FWRAP_HOME")
	if len(fwrapHome) > 0 {
		return fwrapHome
	}

	usr, _ := user.Current()
	return usr.HomeDir+"/.fwrap"
}

func (flutterSdk *FlutterSdk) path() string {
	var dirName string
	if flutterSdk.versionDefined() {
		dirName = flutterSdk.version
	} else {
		dirName = defaultChannel
	}

	return flutterSdk.home()+"/"+dirName
}

func (flutterSdk *FlutterSdk) gitTag() string {
	if !flutterSdk.versionDefined() {
		return defaultChannel
	}
	if version.Compare(flutterSdk.version, "1.17", "<") {
		return "v"+flutterSdk.version
	}
	return flutterSdk.version
}

func (flutterSdk *FlutterSdk) IsInstalled() bool {
	path := flutterSdk.path()
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func (flutterSdk *FlutterSdk) Install() (err error) {
	if (flutterSdk.IsInstalled()) {
		return;
	}

	err = flutterSdk.createFolder()
	if err != nil {
		return
	}

	err = flutterSdk.checkoutSDK()
	if err != nil {
		flutterSdk.deleteVersion()
		return
	}
	err = flutterSdk.initSdk()
	if err != nil {
		flutterSdk.deleteVersion()
		return
	}
	
	color.Green("Flutter installation OK !")
	return
}

func (flutterSdk *FlutterSdk) RunWrappedCommand(args []string) (err error) {
	var commandPath = flutterSdk.path() + "/bin/flutter"
	command := exec.Command(commandPath)
	command.Args = append([]string{commandPath}, args...)
	command.Stderr = os.Stderr
	command.Stdout = os.Stdout
	return command.Run()
}

func (flutterSdk *FlutterSdk) deleteVersion() (err error) {
	color.Cyan("Cleaning up Flutter SDK ...")
	err = os.RemoveAll(flutterSdk.path())
	if err != nil {
		color.Red("Error deleting .flutter directory: " + err.Error())
	}
	return
}

func (flutterSdk *FlutterSdk) createFolder() (err error) {
	err = os.MkdirAll(flutterSdk.path(), os.ModePerm)
	if err != nil {
		color.Red("Error creating .flutter directory: " + err.Error())
	}
	return
}

func (flutterSdk *FlutterSdk) checkoutSDK() (err error) {
	color.Cyan("Checking out Flutter SDK ...")
	command := exec.Command("git", "clone", "-b", flutterSdk.gitTag(), "https://github.com/flutter/flutter.git", flutterSdk.path())
	err = command.Run()
	if err != nil {
		color.Red(err.Error())
	}
	return
}

func (flutterSdk *FlutterSdk) initSdk() (err error) {
	color.Cyan("Installing SDK (Dart SDK & Tools)")
	return flutterSdk.RunWrappedCommand([]string{"--version"})
}