package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:     "fwrap",
	Short:   "Flutter sdk wrapper for easy flutter management.",
	Long:    "Fwrap will help you to fix flutter version by project, reading version in pubspec.yaml.\nYou can use frwap to execute flutter command, replacing [command] by your flutter command.",
	Example: "fwrap fl doctor (Run flutter doctor)\nfwrap fl create . (use latest sdk to create a new project in current directory)",
	Version: "1.2",
}

func Run(args []string) {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
