package cmd

import (
	"os"

	"github.com/spf13/viper"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var flutterSdk FlutterSdk
var versionCmd = &cobra.Command{
	Use:   "fl",
	Short: "Execute a flutter command.",
	Long:  `Executes a flutter command. All parameters after 'fl' are transmitted 'as is' to the appropriate Flutter SDK.`,
	Run: func(cmd *cobra.Command, args []string) {
		flutterSdk = FlutterSdk{readPubspec()}

		var err error
		if !flutterSdk.IsInstalled() {
			err = flutterSdk.Install()
		}
		if err == nil {
			err = flutterSdk.RunWrappedCommand(args)
		}
		if err != nil {
			os.Exit(1)
		}
	},
	DisableFlagParsing: true,
}


func readPubspec() (version string) {
	const pubspecFile = "./pubspec.yaml"

	_, err := os.Stat(pubspecFile)

	if os.IsNotExist(err) {
		color.Yellow("You are not in flutter project, can't find pubspec.yaml file...")
		color.Yellow("Fwrap will use latest SDK version")
		return
	}

	viper.SetConfigFile(pubspecFile)

	if err := viper.ReadInConfig(); err == nil {
		color.Green("pubspec.yaml found (path: " + viper.ConfigFileUsed() + ")")
	}
	environmentMap := viper.GetStringMap("environment")

	yamlVersion, ok := environmentMap["flutter"]
	if !ok {
		color.Yellow("No version specified in pubspec.yaml, using latest stable SDK version")
		return
	}
	version = yamlVersion.(string)
	color.Yellow("Version found in pubspec.yaml: " + version)
	return
}
