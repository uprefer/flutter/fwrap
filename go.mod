module gitlab.com/uprefer/flutter/fwrap

go 1.13

require (
	github.com/fatih/color v1.8.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mcuadros/go-version v0.0.0-20190830083331-035f6764e8d2
	github.com/mitchellh/go-homedir v1.1.0
	github.com/ory/go-acc v0.1.0
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.1
	github.com/src-d/go-git v4.7.0+incompatible
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20200103143344-a1369afcdac7 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1 // indirect
	rsc.io/quote v1.5.2
)
