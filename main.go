package main

import (
	"os"

	"gitlab.com/uprefer/flutter/fwrap/cmd"
)

func main() {
	cmd.Run(os.Args)
}
