[download_macos]: https://gitlab.com/api/v4/projects/16160199/packages/generic/fwrap_darwin/v1.4/fwrap
[download_linux]: https://gitlab.com/api/v4/projects/16160199/packages/generic/fwrap_linux/v1.4/fwrap

# <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/16160199/fwrap.png?width=40" alt="logo" height="100"> fwrap

A **standalone CLI** to use the right Flutter SDK version per project.

[![Download MacOS](https://img.shields.io/badge/download-macos-blue)][download_macos]
[![Download Linux](https://img.shields.io/badge/download-linux-blue)][download_linux]

## :mag: Overview

### :sparkles: Easy to install : Standalone binary

**Fwrap** doesn't need anything else to work. Download the binary and you're ready to go !


### :sparkles: Easy to use :

Simply prefix your `flutter` commands by `fwrap fl`. 

Fwrap will make sure the required Flutter SDK is installed, then it will run your command !

```shell
# Example : Build Android release
./fwrap fl build appbundle --release --build-number=12
```

### :sparkles: Consistent : Flutter version determined by project setup

**Fwrap** checks project's `pubspec.yml` file to determine Flutter required version.

For example, this `pubspec.yml` will use **Flutter v1.12.13+hotfix.9** :
```yml
name: mygreatproject
version: 0.1+0
environment:
  flutter: "1.12.13+hotfix.9"
```

:bulb: If no version is specified, **fwrap** will use latest **stable version**.


### :sparkles: Efficient : shared installations

Flutter SDKs are installed in your home directory : `~/.frwap/<sdk version>`.

So all projects requiring Flutter vX.Y.Z use the same SDK installation.

:bulb: You can choose another directory using environment variable `FWRAP_HOME`.

### :sparkles: Great for CI/CD : 

All this makes it great for CI/CD :
 - **No Dependencies :** A simple `curl -L` and you're ready to go !
 - **Consistent :** Simply uses the right SDK version for your project !
 - **Efficient :** Do not download the same SDK version twice !


## :hammer: Installation

### Linux terminal
```shell
cd <your_project_directory>
curl -L https://gitlab.com/api/v4/projects/16160199/packages/generic/fwrap_linux/v1.4/fwrap -o fwrap && chmod +x ./fwrap 
```

:warning: You might want to add `./fwrap` to your `.gitignore` file.

### MacOS terminal
```shell
cd <your_project_directory>
curl -L https://gitlab.com/api/v4/projects/16160199/packages/generic/fwrap_darwin/v1.4/fwrap -o fwrap && chmod +x ./fwrap 
```

:warning: You might want to add `./fwrap` to your `.gitignore` file.

### Manual Download

You can download **fwrap** binaries [here](https://gitlab.com/uprefer/flutter/fwrap/-/packages)

Save fwrap binary into:

- A flutter project directory
- Any directory within your `$PATH`


## :book: Usage

You can execute all Flutter SDK commands using `fwrap fl` prefix.

### Build flutter app
```shell
# Build Android release
./fwrap fl build appbundle --release --build-number=12
```


### Run flutter app
```shell
# Run Flutter app
./fwrap fl run 
```

### Create new flutter project
```shell
# Create new project into current directory
# This will use the latest stable Flutter SDK
./fwrap fl create --androidx . 
```
